package movie;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MovieSparql extends Base {

    // Directory where we've stored the local data files, such as movie.owl
    public static final String SOURCE = "./src/resources/data/";

    // Movie ontology namespace
    public static final String MOVIE_NS = "http://www.semanticweb.org/anns/ontologies/2016/9/untitled-ontology-5#";

    private static final String query1 = "SELECT ?Actor ?Producer " +
    		"WHERE { movie:Dear_John movie:hasActor ?Actor .\n" +
    		"movie:Dear_John movie:isProducerOf ?Producer }";
    
    private static final String query2 = "SELECT ?Actor ?Producer ?ReleasedYear " +
    		"WHERE { movie:Dear_John movie:hasActor ?Actor ." +
    		"movie:Dear_John movie:isProducerOf ?Producer ." +
    		"movie:Dear_John movie:isReleasedYearOf ?ReleasedYear }";
    
    private static final String query3 = "SELECT ?Actor ?Producer ?ReleasedYear " +
    		"WHERE { movie:Dear_John movie:hasActor ?Actor ." +
    		"movie:Dear_John movie:isProducerOf ?Producer ." +
    		"movie:Dear_John movie:isReleasedYearOf ?ReleasedYear. }" +
    		"ORDER BY DESC (?ReleasedYear) LIMIT 1";
    
    
    private static final String query4 = "SELECT  ?Producer ?Year ?Actor ?MovieName ?Actress ?Rating" +
    		"WHERE { ?MovieName movie:hasActor ?Actor ." +
    		"?MovieName movie:hasProducer ?Producer ." +
    		"?MovieName movie:hasReleasedYear ?Year." +
    		"?MovieName movie:hasActress ?Actress ." +
    		"?MovieName movie:hasrating ?Rating" +
    		" }" +
    		"ORDER BY ASC(?Rating)";
    /***********************************/
    /* Static variables                */
    /***********************************/

    @SuppressWarnings( value = "unused" )
    private static final Logger log = LoggerFactory.getLogger( MovieSparql.class );
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MovieSparql().setArgs(args).run();
	}

	public void run() {
		// TODO Auto-generated method stub
        OntModel m = getModel();
        loadData( m );
        String prefix = "prefix movie: <" + MOVIE_NS + ">\n" +
                        "prefix rdfs: <" + RDFS.getURI() + ">\n" +
                        "prefix owl: <" + OWL.getURI() + ">\n";
        
        showQuery(m,prefix+query1);
        
        showQuery(m,prefix+ query2);
        
        showQuery(m,prefix+ query3);
        
        showQuery(m,prefix+ query4);
	}
	
    protected OntModel getModel() {
        return ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
    }

    protected void loadData( Model m ) {
        FileManager.get().readModel( m, SOURCE + "Assignment1.owl" );
    }

    protected void showQuery( Model m, String q ) {
    		System.out.println(q);
        Query query = QueryFactory.create( q );
        QueryExecution qexec = QueryExecutionFactory.create( query, m );
        try {
            ResultSet results = qexec.execSelect();
            ResultSetFormatter.out( results, m );
        }
        finally {
            qexec.close();
        }

    }

}
